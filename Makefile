BIN ?= nb
BIN_BOOKMARK ?= bookmark
PREFIX ?= ~/.bin

install:
	install $(BIN) $(PREFIX)
	install bin/$(BIN_BOOKMARK) $(PREFIX)
	./$(BIN) env install

uninstall:
	rm -f $(PREFIX)/$(BIN)
	rm -f $(PREFIX)/$(BIN_BOOKMARK)
	./$(BIN) env uninstall
